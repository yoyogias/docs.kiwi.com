FORMAT: 1A
HOST: https://kiwicom-prod.apigee.net/partners

# Checking and accepting price changes

## Checking price changes

You receive info about price change for a specific booking from **Webhooks** API. The result can be similar to the following:

```json
{
  "bid": 3649570,
  "type": "price_changed",
  "mmb_link": "https://www.kiwi.com/en/account/bookings/BID?deeplink=passengers-edit",
  "trigger": "price_changed",
  "price_change": {
    "occurred": "2019-08-08T14:43:10+02:00",
    "currency": "USD",
    "original_price": 1443,
    "new_price": 3067,
    "price_difference": 1414
  }
}
```

* In `mmb_link` you received a link to Manage My Booking (MMB) specific booking ID. It is possible to **confirm a price change manually** through MMB.
* Save the **bid** (booking ID) for use in subchapter [Getting price change ID](https://docs.kiwi.com/mambo/price_changes/#header-getting-a-price-change-id) and for [Accepting a price change](https://docs.kiwi.com/mambo/price_changes/#header-accepting-a-price-change).
* See [Webhooks schema table](https://docs.kiwi.com/mambo/price_changes/#header-webhooks).

### Links

* See the example of the older [1.2 version of Webhooks API](https://docs.kiwi.com/webhooks/1.2/#example-calls-price_changed-post).
* See more about current [2.0 version of Webhooks API](https://docs.kiwi.com/webhooks/2.0/#example-calls-price_changed-post).

## Not accepting a price change

**If you do not want to accept a price change, wait 24 hours to refund the whole booking. If you wish to accept a price change, follow the documentation below.**

## Authorizing

To authorize in Kiwi.com MMB API see the following documentation: [**Authorizing to Kiwi.com API**](https://docs.kiwi.com/authorization)

If you use Webhooks version 2.0 see the subchapter [Accepting a price change](https://docs.kiwi.com/mambo/price_changes/#header-accepting-a-price-change).

---

## Getting a price change ID

#### **(Optional step - for Webhooks version 1.2)**

1. Make a **GET** request with specific `booking ID` (`{booking_id}` called **BID** as well) to the following endpoint. Booking ID (**BID**) is a path parameter identifying a unique reservation. It is always an integer:

    ```
    https://kiwicom-prod.apigee.net/partners/bookings/{insert-bid}/price_changes
    ```

2. In HTTP header use `KW-Auth-Token` as a parameter and received token from authentication response as a value. You have received `KW-Auth-Token` if you followed subchapter [Authorizing](https://docs.kiwi.com/mambo/price_changes/#header-authorizing). A `KW-Auth-Token` is a string similar to this example:

    ```
    ahskdfhklashdflkjasdfklbasdkfbknbsvUUhzIiwiTFViZGhSbEJRcTh2OUcuNGVtY204T0JPSJJSKFLNlamRsRzlNN05Zb3JqSmlwsakbdfas65d45asdaDIzNTU3MTMwNTJd.-3pPQnZE0VkFaasadsdLiNX1s5Sk
    ```

3.  In HTTP header use `apikey` as a parameter and insert your apikey as a value.

4. You receive a response similar to the following. Save the value of `id` (`extra_id`) for a procedure described in the following subchapter [Accepting a price change](https://docs.kiwi.com/mambo/price_changes/#header-accepting-a-price-change). This `id` is unique for a specific price amount. If another price change occurs, another `id` is generated.

    ```json
    {
        "price_changes": [
            {
                "id": 0101010,
                "price": {
                    "amount": "10.00",
                    "base": "10.00",
                    "currency": "EUR",
                    "merchant": "0.0",
                    "service": "0.0",
                    "service_flat": "0.0"
                },
                "status": "expired"
            }
        ]
    }
    ```

See subchapter [Price changes](https://docs.kiwi.com/mambo/price_changes/#header-price-changes) for more information about this response.

---

## Accepting a price change

1. Make a **POST** request to the following endpoint. Insert your **BID** and extra ID from previous steps.

    ```
    https://kiwicom-prod.apigee.net/partners/bookings/{insert-bid}/price_changes/{insert-extra-id}/credit_payments
    ```

2. In HTTP header use `KW-Auth-Token` as a parameter and received token from authentication response as a value. A token is a string similar to this example:

    ```
    ahskdfhklashdflkjasdfklbasdkfbknbsvUUhzIiwiTFViZGhSbEJRcTh2OUcuNGVtY204T0JPSJJSKFLNlamRsRzlNN05Zb3JqSmlwsakbdfas65d45asdaDIzNTU3MTMwNTJd.-3pPQnZE0VkFaasadsdLiNX1s5Sk
    ```

3. In HTTP header use `apikey` as a parameter and insert your apikey as a value.[^1]

* See response schema table of [Confirm payment](https://docs.kiwi.com/mambo/price_changes/#header-confirm-payment).

## Response schema tables

### Webhooks

|Response Field|Type|Description|Example|
|---|---|---|---|
|**bid**|integer|Booking ID|3649570|
|type|string|Describe what notification has been sent|price_changed|
|mmb_link|string|Web link to Manage My Booking (MMB) specific booking ID (allows manual confirmation)|https://www.kiwi.com/en/account/bookings/BID?deeplink=passengers-edit|
|trigger|string|Always has value `price_changed`|price_changed|
|occurred|string|Signifies when a request to send a price change has been created|2019-08-08T14:43:10+02:00|
|currency|string|Currency|USD|
|original_price|integer|Price before the price change|1443|
|new_price|integer|Price after the price change|3067|
|**price_difference**|integer|The difference between new and old price|1414|

### Price changes

|Response Field|Type|Description|Example|
|---|---|---|---|
|id|integer|Identifies specific price change amount|0101010|
|amount|string|Total amount of money expressed in currency|20.00|
|base|string|The base price of a product, usually from the transport provider.|10.00|
|currency|string|Price currency, 3 letter currency code as defined by ISO-4217.|EUR|
|merchant|string|Additional third-party fees like tax etc. Displayed for legal reasons.|5.00|
|service|string|Service fee, known as margin, dynamically calculated from the base price depending on business rules.|2.00|
|service_flat|string|Service flat fee with always the same price added to the base price amount depending on business rules.|3.00|
|status|string|Status of price change request (for all values see table [Price changes request statuses](https://docs.kiwi.com/mambo/price_changes/#header-price-changes-request-statuses))|pending|

### Confirm payment

|Response Field|Type|Description|Example|
|---|---|---|---|
|amount|string|Price change difference from original amount|16,41|
|currency|string|Currency|EUR|
|extra_id|integer|Identifies specific price change event|010101|
|status|string|Status of price change request (for all values see table [Price changes request statuses](https://docs.kiwi.com/mambo/price_changes/#header-price-changes-request-statuses))|pending|

### Price changes request statuses

|Status value|Description|
|---|---|
|open|newly created|
|pending|Price change request processing|
|closed|Price change request processed|
|confirmed|Paid, booking is being processed|
|expired|Option for payment has expired (time depends on category)|
|refunded|Refunded|
|cancelled|Cancelled|
|deleted|Deleted|
|error|Something went really wrong, often written manually|
